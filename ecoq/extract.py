from .quality_attributes import ATTRIBUTES
from .utilities import read_quality_array
import rasterio

def extract_attribute(attribute, quality_array):
    """
    Extracts the bitpattern for the requested attribute from an ECOSTRESS
    quality control science data se (QC SDS)

    Parameters
    ----------

    attribute:
        Quality attribute for ECOSTRESS LSTE observations/estimations

    quality_array:
        The input QC SDS

    Returns
    -------

    A numpy array
    """
    positions = ATTRIBUTES[attribute]['bits']
    pattern_length = int(len(positions) * '1', 2)  # how many bits?, integerized binary string
    rightmost_position = min(positions)  # smallest index, points to starting bit
    return (((quality_array >> rightmost_position) & pattern_length).astype('uint8'))
