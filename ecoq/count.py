from .quality_attributes import CLOUD_OCEAN
from .quality_attributes import ATTRIBUTES
from .extract import extract_attribute
from .utilities import get_quality
import numpy

def count_attribute_qualities(attribute, quality_array):
    """
    This function counts the number of times qualities of an attribute appear
    in the input 'quality_array'

    Parameters
    ----------
    attribute:
        A data quality attribute from the ones hardcoded in ATTRIBUTES

    quality_array:
        A data quality numpy array read from an ECOSTRESS QC SDS

    Returns
    -------
    A dictionary zipping qualities of an attribute with the number of times
    they appear in the input 'quality_array'

    Examples
    --------
    ..
    """
    attribute_array = extract_attribute(attribute, quality_array)
    unique_values, counts = numpy.unique(attribute_array, return_counts=True)
    qualities = [get_quality(attribute, integer_flag) for integer_flag in unique_values]
    frequencies = dict(zip(qualities, counts))
    return frequencies

def count_all_qualities(quality_array):
    """
    This function counts the number of times qualities of each attribute appear
    in the input 'quality_array'

    Parameters
    ----------
    quality_array:
        A data quality numpy array read from an ECOSTRESS QC SDS

    Returns
    -------
    A dictionary zipping qualities of each attribute with the number of times
    they appear in the input 'quality_array'

    Examples
    --------
    ..
    """
    frequencies = {}
    for attribute in ATTRIBUTES:
    # FIXME -- use ATTRIBUTES_WITH_VALID_FLAGS, will result in cleaner code!
    # from .quality_attributes import ATTRIBUTES_WITH_VALID_FLAGS
    # for attribute in ATTRIBUTES_WITH_VALID_FLAGS:
    # ----------------------------------------------------------------------
        if attribute != CLOUD_OCEAN:
            frequencies[attribute] = count_attribute_qualities(attribute, quality_array)
        else:
            frequencies[CLOUD_OCEAN] = ATTRIBUTES[CLOUD_OCEAN]['qualities']
    return frequencies



