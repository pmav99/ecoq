from .quality_attributes import CLOUD_OCEAN
from .quality_attributes import ATTRIBUTES
from .extract import extract_attribute
from scipy import stats

def generate_attribute_statistics(attribute, quality_array):
    """
    """
    attribute_array = extract_attribute(attribute, quality_array)
    return stats.describe(attribute_array)


def generate_statistics(quality_array):
    """
    """
    statistics = {}
    for attribute in ATTRIBUTES:
    # FIXME -- use ATTRIBUTES_WITH_VALID_FLAGS, will result in cleaner code!
    # from .quality_attributes import ATTRIBUTES_WITH_VALID_FLAGS
    # for attribute in ATTRIBUTES_WITH_VALID_FLAGS:
    # ----------------------------------------------------------------------
        if attribute != CLOUD_OCEAN:
            statistics[attribute] = generate_attribute_statistics(attribute, quality_array)
        else:
            statistics[CLOUD_OCEAN] = ATTRIBUTES[CLOUD_OCEAN]['qualities']
    return statistics
