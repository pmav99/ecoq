from .constants import *
from .quality_attributes import ATTRIBUTES
from .quality_attributes import ATTRIBUTES_WITH_VALID_FLAGS
from .count import count_attribute_qualities
from .count import count_all_qualities
from .statistics import generate_attribute_statistics
from .statistics import generate_statistics
import rasterio
import numpy
from .query import query_pixel_value
from .extract import extract_attribute
from .extract import read_quality_array
from .utilities import write_attribute_map
from .bitpatterns import filter_requested_qualities

def list_attributes(attributes, complete=False):
    """
    This function lists the requested data quality attributes as they are
    hardcoded in ATTRIBUTES

    Parameters
    ----------
    attributes:
        A list of attributes among the keys hardcoded in the dictionary ATTRIBUTES.
        See also quality_attributes.py

    Returns
    -------
    quality_attributes:
        A list containing the requested attributes from the dictionary ATTRIBUTES

    Examples
    --------
    ..
    """
    quality_attributes = {}

    try:
        if complete:  # list attributes and qualities
            for attribute in ATTRIBUTES:
                title = ATTRIBUTES[attribute]['title']
                qualities = ATTRIBUTES[attribute]['qualities']
                quality_attributes[title] = qualities
            return quality_attributes

        if not attributes:  # list only attributes
            return list(ATTRIBUTES.keys())

        if attributes:
            attributes = (string.lower() for string in attributes)  # thus accept capitalised strings
            for attribute in attributes:
                title = ATTRIBUTES[attribute]['title']
                quality_attributes[attribute] = ATTRIBUTES[attribute]['flags']
            return quality_attributes
    except KeyError:
        return f"'{attribute}' is not a valid quality attribute name!"


def count_qualities(attribute, quality_map):
    """
    This function counts the number of times each unique quality class of the
    requested data quality 'attribute' appears in the 'quality_map'.

    Parameters
    ----------
    attribute:
        A data quality attribute within an ECOSTRESS QC SDS

    quality_map:
        An ECOSTRESS QC SDS

    Returns
    -------
    A dictionary with frequency statistics for qualities (quality categories)
    of data quality attributescontained in an ECOSTRESS QC SDS
    """
    quality_array, array_profile = read_quality_array(quality_map)

    if attribute == 'all':
        frequencies = count_all_qualities(quality_array)

    else:
        frequencies = {}
        for item in attribute:
            frequencies[item] = count_attribute_qualities(item, quality_array)

    return frequencies


def generate_quality_statistics(attributes, quality_map):
    """
    """
    quality_array, array_profile = read_quality_array(quality_map)

    if attributes == 'all':
        statistics = generate_statistics(quality_array)

    else:
        statistics = {}
        for attribute in attributes:
            statistics[attribute] = generate_attribute_statistics(attribute, quality_array)

    return statistics
    pass


def filter_quality_attribute(
        quality_map,
        quality,
        output,
        print_unique_values,
        quiet,
        ):
    """
    This function filters a Quality Control Science Data Set (here, the
    'quality_map') for the requested 'quality'

    Parameters
    ----------
    quality_map:
        An ECOSTRESS QC SDS

    quality:
        A specific data quality attribute (and class) in the form of a composite
        string <attribute:binary_quality_flag>, where:
        - attribute is any of the hardcoded attributes in
          ATTRIBUTES_WITH_VALID_FLAGS
        - binary_quality_flag is any of the corresponding binary flags to the
          requested attribute, as hardcoded in ATTRIBUTES_WITH_VALID_FLAGS

    output:
        The output filename to write the filtered quality map

    print_unique_values:
        A boolean flag whether to print unique quality integer values that...

    quiet:
        A boolean flag to adjust verbosity

    Returns
    -------
    This function either writes a map containin the requested filtered values,
    thus it does not return any data structure, or it returns the output of
    filter_requested_qualities()

    Examples
    --------
    ..

    """
    quality_array, array_profile = read_quality_array(quality_map)
    array_profile.update(dtype=OUTPUT_DTYPE)
    array_profile.update(transform=rasterio.transform.guard_transform(array_profile['transform']))
    output = quality[0] + OUTPUT_EXTENSION
    return NOT_IMPLEMENTED

    filtered_quality_array = filter_requested_qualities(
            quality_array,
            quality,
            )
    if not print_unique_values:
        pass
        # write_attribute_map(
        #         output_file=output,
        #         profile=array_profile,
        #         attribute=quality[0],
        #         attribute_map=attribute_map,
        #         quiet=quiet,
        #         )
    else:
        unique_quality_values = numpy.unique(filtered_quality_array)
        return unique_quality_values

def extract_attributes(
        quality_map,
        attributes,
        output,
        all_attributes=False,
        output_directory=None,
        quiet=False,
        ):
    """
    This functions extracts the requested data quality attribute from an
    ECOSTRESS QC SDS in a separate (georeferenced?) raster map

    Parameters
    ----------
    quality_map:
        An ECOSTRESS QC SDS

    attribute:
        One (or multiple) data quality attribute(s) within an ECOSTRESS QC SDS

    output:
        Output filename for the extracted data quality attribute

    all_attributes:
        Boolean flag, if True all attributes are extracted each in a separate
        raster map

    output_directory:
        Output directory for the extracted data quality attribute raster map(s)

    quiet:
        Boolean, if True print out messages in the console are supressed

    Returns
    -------
    This function does not return anything
    """
    quality_array, array_profile = read_quality_array(quality_map)
    array_profile.update(dtype=OUTPUT_DTYPE)
    array_profile.update(transform=rasterio.transform.guard_transform(array_profile['transform']))

    if all_attributes or 'all' in set(attributes):
        attributes = ATTRIBUTES_WITH_VALID_FLAGS

    for attribute in attributes:
        output = attribute + OUTPUT_EXTENSION
        attribute_map = extract_attribute(attribute, quality_array)
        write_attribute_map(
                output_file=output,
                profile=array_profile,
                attribute=attribute,
                attribute_map=attribute_map,
                quiet=quiet,
                )
