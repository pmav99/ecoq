from .quality_attributes import ATTRIBUTES

def extract_bitpattern(decimal, positions):
    """
    Extracts the bitpattern from a 'decimal' located between
    the requested 'positions'

    Parameters
    ----------

    decimal:
        Decimal value from whose binary form to extract the requested bitpattern

    positions:
        A tuple listing the positional indices of bits in a bitpattern.
        The function computes internally the length of the requested
        sub-bitpattern based on the number of indices given.
        Indexing bit positions starts at 0.

    Returns
    -------

    A integer

    Examples
    --------
    >>> extract_bitpattern(44742, (15,14))
    2

    Another example
    >>> extract_bitpattern(16399, (3,2))
    3

    Another example with more bits
    >>> extract_bitpattern(16399, (3,2,1,0))
    15
    """
    selector = int(len(positions) * '1', 2)  # which bits?
    starting_bit = min(positions)  # smallest index points to starting bit
    return (((decimal >> starting_bit) & selector))


def integerize_attribute_binary(attribute_binary):
    """
    This functions converts the input 'attribute_binary' string into an integer
    and performs a bitwise left shift by the number of 'bits' reserved for the
    attribute in question

    Parameters
    ----------
    attribute_binary:
        An 'attribute_binary' string of the form '<attribute>:<binary>' where:
            - 'attribute' may be any of the valid attributes hardcoded in
              ATTRIBUTES_WITH_VALID_FLAGS

            - 'binary' may be any of the binary quality flags for an attribute
              hardcoded in ATTRIBUTES_WITH_VALID_FLAGS[attribute]['flags']

            Examples to explain the bitwise math performed here-in:

            The "quality" integer form of the binary string '10' shifted to the
            left by 14 is 32768.
            The binary '11' shifted to the left by 12 is 12288.

    Returns
    -------
    An integer that encodes the requested attribute and quality flag

    Examples
    --------
    ..
    """
    attribute, binary = attribute_binary.split(':')
    shifting = min(ATTRIBUTES[attribute]['bits'])
    return int(binary, 2) << shifting


def integerize_attribute_selector(attribute):
    """
    This function computes an integer that acts as a selector for the bit
    positions of an attribute among the ones hardcoded in
    ATTRIBUTES_WITH_VALID_FLAGS

    Parameters
    ----------
    attribute

    Returns
    -------
    An integer that selects the bit positions, for the requested attribute,
    in a 16-bit encoded integer
    """
    bits = ATTRIBUTES[attribute]['bits']
    positions = len(bits)
    selector = int(positions * '1', 2)  # which bits?
    shifting = min(bits)
    return selector << shifting


def filter_requested_qualities(quality_array, requested_qualities):
    """
    This function filters an ECOSTRESS QC SDS and retains the requested
    quality flags

    Parameters
    ----------
    quality_array:
        An ECOSTRESS QC SDS raster map

    qualities
        A list of "quality" integers. Consult the ECOSTRESS LSTE Product
        Specification manual or the ATTRIBUTES dictionay defined in
        `quality_attributes.py`.

    Returns
    -------
        # A numpy array containing the requested quality integer pixel values
        A boolean numpy array containing 'True' for the requested quality integer
        pixel values

    Examples
    --------
    >>> import rasterio
    >>> geotiff = rasterio.open("ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01_QC_Compressed.tif")
    >>> quality = geotiff.read(1,)
    >>> filter_requested_qualities(quality, ['mandatory:00'])
    array([0, 0, 0, ..., 0, 0, 0], dtype=uint16)
    """
    quality_integers = []
    masking_integers = []
    for attribute_binary in requested_qualities:
        quality_integer = integerize_attribute_binary(attribute_binary)
        quality_integers.append(quality_integer)
        attribute = attribute_binary.split(':')[0]
        masking_integer = integerize_attribute_selector(attribute)
        masking_integers.append(masking_integer)
    integer = sum(quality_integers)  # compose integer for requested qualities
    masked_integer = sum(masking_integers)
    quality_array = quality_array & masked_integer
    return quality_array[quality_array == integer]


def filter_array(array, filtering_array):
    """
    This function filters an input 'array' based on a boolean 'filtering_array'

    Parameters
    ----------
    array:
        An input numpy array as returned by rasterio.open(raster).read(1,)

    filtering_array:
        A boolean numpy array based on which to filter 'array'

    Returns
    -------
    array:
        A numpy array of shape identical to the input 'array' though filtered
        by the 'filtering_array'
    """
    # product = rasterio.open(ecostress_product)
    # array = product.read(1,)
    array = numpy.ma.masked_values(array, 0)  # this is a Masked array
    array.data[~array.mask] == 0  # Zerofy... ?
    filtering_mask = filtering_array & ~array.mask  # CHECK-ME!
    array.mask = filtering_mask  # This array contains input data + final mask
    return array
