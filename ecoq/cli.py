from .messages import QUALITY_ATTRIBUTES
import functools
import click
from .quality_attributes import ATTRIBUTES
from .quality_attributes import ATTRIBUTES_WITH_VALID_FLAGS
from .query import query_pixel_value
from .query import query_pixel_qualities
from . import api
import pprint

@click.group()
def cli():
    pass

def common_options(f):
    options = [
        click.argument('quality_map',
                        type=click.Path(
                            exists=True,
                            dir_okay=False,
                            )
                      ),
        click.argument('attribute', required=True, nargs=-1),
        click.option("-o",
                     "--output",
                     type=click.Path(exists=False,
                         dir_okay=False,
                         resolve_path=True),
                     help="Output [CSV] file",
                    ),
        click.option("-d",
                     "--output-directory",
                     type=click.Path(
                         exists=False,
                         dir_okay=True,
                         resolve_path=True),
                     help="Output [CSV] file",
                    ),
        click.option("-q",
                     "--quiet",
                     is_flag=True,
                     default=False,
                     help="Suppress progress bars and messages",
                    ),
    ]
    return functools.reduce(lambda x, opt: opt(x), options, f)


@cli.command('list')
@click.argument('attribute', required=False, nargs=-1)
@click.option('-c', '--complete', is_flag=True, default=False, help="Print complete list of quality attributes and flags")
def list_quality_attributes(attribute, complete=False):
    """List data quality attributes"""
    quality_attributes = api.list_attributes(
            attributes=attribute,
            complete=complete,
            )
    if not attribute:
        click.echo(QUALITY_ATTRIBUTES)
    if type(quality_attributes) == dict:
        click.echo(pprint.pprint(quality_attributes))
    else:
        click.echo(quality_attributes)

@cli.command('count')
# @common_options
@click.argument('attribute', required=False, nargs=-1)
@click.argument('quality_map',
        type=click.Path(
            exists=True,
            dir_okay=False,
            )
        )
@click.option("-a", "--all-attributes", is_flag=True, default=False, help="Extract all QA variables in separate GTiff files")
def count_attribute_qualities(
        quality_map,
        attribute,
        all_attributes=False,
        output=None,
        output_directory=None,
        quiet=False,
        ):
    """
    Count attribute quality classes
    """
    if not attribute or all_attributes or 'all' in set(attribute):
        attribute = 'all'
    frequencies = api.count_qualities(attribute, quality_map)
    click.echo(pprint.pformat(frequencies, indent=2))


@cli.command('statistics')
# @common_options
@click.argument('attribute', required=False, nargs=-1)
@click.argument('quality_map',
        type=click.Path(
            exists=True,
            dir_okay=False,
            )
        )
@click.option("-a", "--all-attributes", is_flag=True, default=False, help="Extract all QA variables in separate GTiff files")
def generate_attribute_statistics(
        quality_map,
        attribute,
        all_attributes=False,
        output=None,
        output_directory=None,
        quiet=False,
        ):
    """
    Generate descriptive statistics for attribute qualities
    """
    if not attribute or all_attributes or 'all' in set(attribute):
        attribute = 'all'
    statistics = api.generate_quality_statistics(
            attributes=attribute,
            quality_map=quality_map,
    )
    click.echo(pprint.pformat(statistics, indent=2))


@cli.command('query')
@click.argument('attribute', required=False, nargs=-1)
@click.argument('quality_map', type=click.Path(exists=True, dir_okay=False))
@click.argument('ordinate', type=click.IntRange(min=0))
@click.argument('abscissa', type=click.IntRange(min=0))
@click.option("-b", "--print-binary", is_flag=True, default=False, help="Print the binarized decimal")
@click.option("-s", "--suppress-qualities", is_flag=True, default=False, help="Suppress printing qualities")
@click.option("-a", "--all-attributes", is_flag=True, default=False, help="Extract all QA variables in separate GTiff files")
@click.option("-q", "--quiet", is_flag=True, default=False, help="Suppress progress bars and messages")
def query_quality_value(
        quality_map,
        attribute,
        abscissa,
        ordinate,
        all_attributes=False,
        print_binary=False,
        suppress_qualities=False,
        quiet=False,
        ):
    """Query a quality control pixel at (x, y)"""
    # if attribute not in set(ATTRIBUTES):
    #     error = "The requested attribute '{a}' does not exist!".format(a=attribute)
    #     raise ValueError(error)
    quality_pixel_value = query_pixel_value(
                                            raster=quality_map,
                                            column=abscissa,
                                            row=ordinate,
                                            )

    message = int('{integer}'.format(integer=quality_pixel_value))
    if not quiet:
        message = "Pixel value: " + str(message)

    if print_binary:
        binary = (bin(quality_pixel_value)[2:])
        message += " [{binary}]".format(binary=binary)
    click.echo(pprint.pformat(message))

    if not suppress_qualities:
        pixel_qualities = query_pixel_qualities(quality_pixel_value)

        from .quality_attributes import CLOUD_OCEAN
        if CLOUD_OCEAN in set(attribute):
            click.echo(ATTRIBUTES[CLOUD_OCEAN]['qualities'])
            return

        if all_attributes or 'all' in set(attribute) or not attribute:
            attribute = ATTRIBUTES_WITH_VALID_FLAGS

        # output reads nicer with 'title's!
        pixel_quality = {
                ATTRIBUTES[item]['title']: pixel_qualities[item]
                for item in attribute
                }
        if not quiet:
            return click.echo(pprint.pformat(pixel_quality, indent=2))
        else:
            for quality in pixel_quality.values():
                click.echo(quality)

@cli.command('filter')
@click.argument('quality', required=True, nargs=-1)
@click.argument('quality_map', type=click.Path(exists=True, dir_okay=False))
@click.option("-o", "--output", type=click.Path(exists=False, dir_okay=False, resolve_path=True), help="Output [CSV] file",)
@click.option("-p", "--print-unique-values", is_flag=True, default=False, help="Print the unique values")
@click.option("-q", "--quiet", is_flag=True, default=False, help="Suppress progress bars and messages")
# @common_options
def filter_quality_map(
        quality_map,
        quality,
        output=None,
        print_unique_values=False,
        quiet=False,
        ):
    """
    Filter quality attributes based on user requested flags
    """
    filtered_quality_attribute = api.filter_quality_attribute(
            quality_map,
            quality,
            output,
            print_unique_values,
            quiet,
            )
    import numpy
    unique_values = numpy.unique(filtered_quality_attribute)
    click.echo(unique_values)


@cli.command('extract')
@common_options
@click.option("-a", "--all-attributes", is_flag=True, default=False, help="Extract all QA variables in separate GTiff files")
def extract_quality_attributes(
        quality_map,
        attribute,
        output=None,
        all_attributes=False,
        output_directory=None,
        quiet=False,
        ):
    """Extract quality attribute in individual raster map"""
    api.extract_attributes(
             quality_map,
             attributes=attribute,  # note attributes vs. attribute
             output=output,
             all_attributes=all_attributes,
             output_directory=output_directory,
             quiet=quiet
             )
    # check if raster map(s) are written?
