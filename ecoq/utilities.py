from .quality_attributes import CLOUD_OCEAN
from .quality_attributes import ATTRIBUTES
import rasterio
import click

def read_quality_array(quality_map):
    """
    Import QC SDS GTiff using 'rasterio'
    """
    # Add some checks!
    # Is it a GDAL-supported format?
    # Is it an ECOSTRESS Quality Control SDS
    # Is it georeferenced?  - Issue warning
    with rasterio.open(quality_map) as qm:
        quality_array = qm.read(1)
        profile = qm.profile
    return quality_array, profile

def get_quality(attribute, integer_flag):
    """
    This function returns the quality (category) of an attribute for a specific
    integer quality flag.
    """
    flag = (bin(integer_flag)[2:].zfill(2))
    # FIXME -- use ATTRIBUTES_WITH_VALID_FLAGS in count_attribute_qualities,
    # will result in cleaner code!
    # ----------------------------------------------------------------------
    if attribute == CLOUD_OCEAN:
        pass
    else:
        if flag not in ATTRIBUTES[attribute]['flags'].keys():
            raise ValueError('The requested quality integer flag {i} does not exist!'.format(i=flag_integer))
    return ATTRIBUTES[attribute]['flags'][flag]

def write_attribute_map(
        output_file,
        profile,
        attribute,
        attribute_map,
        quiet=False,
        ):
    """
    Write a raster map (by default as a) GeoTiff file
    """
    with rasterio.open(output_file, 'w', **profile) as output:
        attribute_map = ( attribute_map * 255).astype('uint8')
        output.write(attribute_map, 1)

        if not quiet:
            message = "'{attribute}' quality flags extracted to '{output}'"
            attribute = ATTRIBUTES[attribute]['title']
            click.echo(message.format(attribute=attribute, output=output_file), err=True)
            # return output
