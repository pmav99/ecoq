from setuptools import setup

setup(
    name='ecoq',
    version='0.1',
    description='Extract data quality attributes from an ECOSTRESS LSTE QC Scientific Data Set',
    url='',
    author='Nikos Alexandris',
    author_email='Nikos.Alexandris@ec.europa.eu',
    license='',
    packages=['ecoq'],
    zip_safe=False,
    install_requires=[
        'Click',
        'numpy',
        'rasterio',
        'pprint',
        'tqdm',
    ],
    entry_points='''
        [console_scripts]
        ecoq=ecoq.cli:cli
    ''',
)
