# Changelog

(
A useful project change log answers three questions:

- Whether it is critical to update
- Whether the release is backwards compatible
- What has changed and why
)

## [Unreleased]

### Added 2020-02-26

- `ecoq count` (renamed from `ecoq statistics`)
- `ecoq statistics` stub for a real statistics retrieving cli subcommand
- Renamed API functions:
  - count_attribute_qualities() from generate_attribute_statistics()
  - list_attributes() from list_quality_attributes()
  - extract_attributes() from extract_quality_attributes()

### Added 2020-02-21

- `ecoq statistics` (renamed from `ecoq statistics` which was only a stub!)
- `ecoq query` -- not yet implemented
- `ecoq filter` -- not yet implemented

## [0.1]

### Added

- `ecoq` functionalities: list, extract
  - planned: query, summary, plot

# References

## Types of changes

**Added** for new features.
**Changed** for changes in existing functionality.
**Deprecated** for soon-to-be removed features.
**Removed** for now removed features.
**Fixed** for any bug fixes.
**Security** in case of vulnerabilities.

## Sources

- [keepachangelog.com](https://keepachangelog.com/en/1.0.0/)
- https://semver.org/ -- _although not sure `ecoq` follows it_ :O
