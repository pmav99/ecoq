## `ecoq`

`ecoq` (read _**ECO**STRESS **q**uality_) assissts the use of Quality Control
Scientific Data Sets (QC SDS) in [ECOSTRESS] Temperature (T) and ε (Emissivity)
[LSTE][] products.

[LSTE]: https://ecostress.jpl.nasa.gov/downloads/psd/ECOSTRESS_SDS_PSD_L2_ver1-1.pdf "Level 2 Product Specification Document"

<!-- [QC SDS]: **Q**uality **C**ontrol **S**cientific **D**ata **S**et -->


>[ECOSTRESS][] (**ECO**system **S**paceborne **T**hermal **R**adiometer **E**xperiment on **S**pace **S**tation) is a thermal infrared experimental mission mounted on the [ISS][]'
>[Kibo][] module. Its scan is perpendicular to the ISS velocity.
>
><figure>
>    <img
>        src="images/ECOSTRESS-In-Container.png" height=180
>        alt="ECOSTRESS radiometer in container (credit: NASA/JPL)"
>    />
>    <figcaption>
>        <a href="https://ecostress.jpl.nasa.gov/mission/ECOSTRESSInContainer.png/view">ECOSTRESS
>        radiometer in container (credit: NASA/JPL)</a>
>    </figcaption>
></figure>

Data quality attributes report on surface, atmospheric, and sensor conditions.
They enable users to obtain a summary of algorithm statistics in a spatial
context. Consequently they may assist in determining the usefulness of
observations.

Pixels in a QC SDS contain unsigned integers that represent bit-packed
combinations of various retrieval conditions such as surface type (land or
ocean), atmospheric conditions (i.e., water vapor content: dry, moist, very
humid, etc.), and cloud cover (and in the case of LSTE products, overall
performance of the TES algorithm).

`ecoq` can selectively:

- `list` data quality attributes and flags
- `extract` quality attributes in raster maps
- `query` a quality control pixel at (x, y)
- `count` frequencies of quality flags per attribute
- generate descriptive `statistics`  for attribute qualities
- `filter` quality attributes based on user requested flags [**Under development**]

[ECOSTRESS]: https://ecostress.jpl.nasa.gov/ "ECOSTRESS"
[ECOSTRESS mission]: https://ecostress.jpl.nasa.gov/mission "ECOSTRESS Mission"
[ISS]: https://en.wikipedia.org/wiki/International_Space_Station "Internaltional Space Station"
[Kibo]: https://en.wikipedia.org/wiki/Kibo_(ISS_module) "Kibo (ISS module)"


## Install

`ecoq` strives to grow [up] and become a proper Python package.
It is recommended to install it inside a virtual environment.

```
python -m venv ecoq_virtualenv
source ecoq_virtualenv/bin/activate
pip install -U pip
pip install git+https://gitlab.com/thermopolis/public/ecoq
```
or if asked, add the `--user` option, i.e.
```
pip install -U pip --user
pip install git+https://gitlab.com/thermopolis/public/ecoq --user
```


## Examples

`ecoq` features several subcommands.
Executing
`ecoq --help`
or even `ecoq`
will present subcommands,
optional flags
and common arguments.

### `ecoq list`

First,
one can list the quality _categories_
(also and simply referred to as _qualities_)
for a single data quality attribute with the `list` subcommand.
For example,
```bash
ecoq list Mandatory
```
will return
```bash
{'00': 'Best quality',
 '01': 'Nominal quality',
 '10': 'Cloud detected',
 '11': 'Pixel not produced'}
```

We are presented with
4 qualities for the 'mandatory' attribute.
Likewise,
all other attributes feature 4 quality categories.


> Note also that the above example shows that
even capitalised attribute strings
(in which case '_Mandatory_')
are handled gracefully by `ecoq`.

### `ecoq statistics`

Counting the frequency of qualities
for the `mandatory` attribute via
```bash
ecoq statistics mandatory ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01_QC.tif
```
will return
```bash
{ 'Best quality': 24559121,
  'Cloud detected': 4151541,
  'Nominal quality': 1431186,
  'Pixel not produced': 270952}
```

### `ecoq query`

The following example
querying a single pixel
from a Quality Control SDS via
```bash
ecoq query ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01_QC.tif 1000 1000
```
will return
```bash
'Pixel value: 20165'
{ 'Atmospheric Opacity': '0.1 - 0.2 (Nominal value)',
  'Data': 'Missing stripe pixel in bands 1 and 5',
  'Emissivity accuracy': '>0.02 (Poor performance)',
  'Iterations': 'Fast convergence',
  'LST accuracy': '1.5 - 2 K (Marginal performance)',
  'MMD': '<0.03 (Vegetation, snow, water, ice)',
  'Mandatory': 'Nominal quality'}
```

Queries,
as almost every subcommand,
can target one or multiple attributes.
For example
```bash
ecoq query mmd ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01_QC.tif 1000 1000
'Pixel value: 20165'
{'MMD': '<0.03 (Vegetation, snow, water, ice)'}
```
and
```bash
ecoq query mmd iterations ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01_QC.tif 1000 1000
'Pixel value: 20165'
{ 'Iterations': 'Fast convergence',
  'MMD': '<0.03 (Vegetation, snow, water, ice)'}
```

### `ecoq extract`

Extract all qualities of an attribute
in a separate GeoTiff raster map:
```bash
ecoq extract mmd ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01_QC.tif
'MMD' quality flags extracted to 'mmd.tif'
```

or all attributes in separate GeoTiff raster maps in one go:
```bash
 ecoq extract all ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01_QC.tif
'Mandatory' quality flags extracted to 'mandatory.tif'
'Data' quality flags extracted to 'data_quality.tif'
'Cloud/Ocean' quality flags extracted to 'cloud_ocean.tif'
'Iterations' quality flags extracted to 'iterations.tif'
'Atmospheric Opacity' quality flags extracted to 'atmospheric_opacity.tif'
'MMD' quality flags extracted to 'mmd.tif'
'Emissivity accuracy' quality flags extracted to 'emissivity_accuracy.tif'
'LST accuracy' quality flags extracted to 'lst_accuracy.tif'
```

The above can be tasked also via the `-a` option like
```
ecoq extract -a ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01_QC.tif
```

## Python API

`ecoq` features an API


```
ecoq.list_attributes
ecoq.count_qualities
ecoq.generate_quality_statistics
ecoq.filter_quality_attribute
ecoq.extract_attributes
```

[**Update API Documentation**]

##

> `ecoq` is developed within the activities of the [THERMOPOLIS]() research project
>
> `ecoq` is inspired more or less by similar projects:
>   - https://github.com/mapbox/rio-l8qa
>   - https://github.com/haoliangyu/pymasker
>   - https://gitlab.com/NikosAlexandris/i.landsat8.qa

## Acknowledgments

Tips & Hints while building `ecoq` by:

- https://gitlab.com/matteopcc
- https://gitlab.com/pmav99
